// interactive list
function Comment(title, comment) {
    this.title = title;
    this.comment = comment;
    this.time = Math.floor(Date.now() / 1000)
}

let comments = [];


document.getElementById('button').onclick = function () {
    let commentName = document.getElementById('comment').value;
    let commentBody = document.getElementById('body').value;

    if (commentName && commentBody) {
        let newComment = new Comment(commentName, commentBody);
        comments.push(newComment);
        showComments();
        comment.value = '';
        body.value = '';
    } else {
        alert("Необходимо заполнить оба поля!");
    }
}
//вывод содержимого на блоке 'commentField'
function showComments() {
    let commentField = document.getElementById('comment-inner')
    let out = '';
    for (let item of comments) {
        out += `<p class = 'comment-inner__time'><em>${timeConverter(item.time)}</em></p>`;
        out += `<h2 class = 'comment-inner__title'>${item.title}</h2>`;
        out += `<p class = 'comment-inner__body'>${item.comment}</p>`;
    }
    ;
    commentField.innerHTML = out;
}
//создание 'timeConverter' для вывода времени комментария
function timeConverter(UNIX_timestamp) {
    let a = new Date(UNIX_timestamp * 1000);
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let hour = a.getHours();
    let min = a.getMinutes();
    let sec = a.getSeconds();
    let time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}




