// animation

//положение круга
let posX = 240;
let posY = 94;

let moveLeft = false;
let moveUp = false;
let circle = document.getElementById('circle');
let box = document.getElementById('animation-box');
box.addEventListener("click", removeEvent);
function removeEvent () {
    setInterval(moveCircle, 3);
    box.removeEventListener('click', removeEvent);
}


//Определяем будущее движение и осткоки от стенок
function moveCircle() {
    switch (posX) {
        //когда шарик доходит до левой границы он поменяет направление вправо,так как у нас moveLeft задан как false,а здесь мы меняем на true
        case 480:
            moveLeft = true;
            break;
        case 0:
            moveLeft = false;
            break;
    }

    switch (posY) {
        //когда шарик доходит до верхней границы он поменяет направление вниз,так как у нас moveUp задан как false,а здесь мы меняем на true
        case 188:
            moveUp = true;
            break;
        case 0:
            moveUp = false;
            break;
    }

    //запуск движений
    moveLeft ?
        //если влево меняем значения на -,если вправо добавляем значения с +
        posX -=3 : posX +=3;
    moveUp ?
        //если вверх меняем значения на -,если вниз добавляем значения с +
        posY -=1 : posY +=1;
    //чтобы отобразить движения на html странице оращаемся к атрибуту и меняем position(так как у нас position:absolut)
    circle.style.left = posX +'px';
    circle.style.top = posY +'px';

}